/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Back;

import Animalitos.Vaca;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Alex
 */
public class aV {
    
    conexiónBDD con = new conexiónBDD();
    
    Connection c;
    PreparedStatement pst;
    ResultSet rs;

    public boolean consultaranimalitos(int identificador){
        String sql = "SELECT * FROM animalitos WHERE identificador="+identificador;
        
        try{
            c = con.getConexion();
            pst = c.prepareStatement(sql);
            rs = pst.executeQuery();
            
         if(rs.next()){
             System.out.println("El número identificador de la vaca es: "+rs.getString("identificador"));
             System.out.println("El nombre de la vaca es: "+rs.getString("nombre"));
             System.out.println("El color de la vaca es: "+rs.getString("color"));
             System.out.println("El genero de la vaca es: "+rs.getString("genero"));
         }else{  
             return false;
         }
            
            
        }catch (SQLException ex){
            ex.printStackTrace();
            System.out.println("Error al consultar la información");
        }
        return true;
        
    }
    
        public boolean agregaranimalitos(Vaca Vaca){
        
        String sql = "INSERT INTO animalitos (identificador, nombre, color, genero) VALUES(?, ?, ?, ?)";
        
        
        try{
            c = con.getConexion();
            pst = c.prepareStatement(sql);
            
            pst.setInt(1, Vaca.getIdentificador());
            pst.setString(2, Vaca.getNombre());
            pst.setString(3, Vaca.getColor());
            pst.setString(4, Vaca.getGenero());
            pst.executeUpdate();
                    
        }catch (SQLException ex){
            ex.printStackTrace();
            System.out.println("Error al agregar la información");
            
        }
        return true;
        
    }
        
        public boolean eliminaranimalitos(String identificador){
            
            String sql = "DELETE FROM animalitos WHERE identificador="+"'"+identificador+"'";
            
            try{
                    c = con.getConexion();
                    pst = c.prepareStatement(sql);
                    pst.executeUpdate();
                    
        }catch (SQLException ex){
            ex.printStackTrace();
            System.out.println("Error al eliminar la información");
            
        }
        return true;
    }
    
    
}
